const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;

const mongoConnect = callback => {
    MongoClient.connect('mongodb+srv://tuetd:6q68Mjjiwvr4I3Tc@cluster0.1zi7y.mongodb.net/shop?retryWrites=true&w=majority')
        .then((client) => {
            console.log('Conneted Mongo Altas');
            _db = client.db();
            callback(client)
        })
        .catch(err => {
            console.log(err)
            throw err;
        })
}

const getDb = () => {
    if (_db) {
        return _db
    }
    throw 'No db found!'
}

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;
