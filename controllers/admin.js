const Product = require('../models/product');

exports.getAddProduct = (req, res, next) => {
    res.render('admin/edit-product', {
        pageTitle: 'Add Product',
        path: '/admin/add-product',
        activeAddProduct: true,
        editing: false
    });
};

exports.postAddProduct = (req, res, next) => {
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const price = req.body.price;
    const description = req.body.description;

    const product = new Product(title, price, imageUrl, description);

    product.save()
        .then({
            title: title,
            price: price,
            imageUrl: imageUrl,
            description: description
        })
        .then(result => {
            console.log('Product added successfully');
            return res.redirect('/admin/products')
        })
        .catch(err => {
            console.log(err)
        })
};

// exports.getEditProduct = (req, res, next) => {
//     const id = req.params.id
//     const editMode = req.query.edit;
//     if (!editMode) {
//         return res.redirect('/')
//     }
//     req.user.getProducts({ where: { id: id } })
//         .then(products => {
//             if (products.length > 0) {
//                 res.render('admin/edit-product', {
//                     pageTitle: 'Edit Product',
//                     path: '/admin/edit-product',
//                     editing: editMode,
//                     product: products[0]
//                 });
//             }
//             return res.redirect('/')
//         })
//         .catch(err => console.log(err))

// };

// exports.postEditProduct = (req, res, next) => {
//     const id = req.body.id
//     const updatedTitle = req.body.title;
//     const updatedImageUrl = req.body.imageUrl;
//     const updatedPrice = req.body.price;
//     const updatedDescription = req.body.description;

//     Product.findById(id)
//         .then(product => {
//             product.title = updatedTitle;
//             product.imageUrl = updatedImageUrl;
//             product.price = updatedPrice;
//             product.description = updatedDescription

//             return product.save()
//         })
//         .then(result => {
//             console.log('Product updated!')
//             res.redirect('/admin/products');
//         })
//         .catch(err => {
//             console.log(err)
//         })
// }

// exports.getProducts = (req, res, next) => {
//     req.user.getProducts()
//         .then(products => {
//             res.render('admin/products', {
//                 prods: products,
//                 pageTitle: 'Admin Products',
//                 path: '/admin/products',
//                 activeShop: true
//             });
//         })
//         .catch(err => {
//             console.log(err)
//         });
// }

// exports.deleteProduct = (req, res, next) => {
//     const id = req.body.id;

//     Product.findById(id)
//         .then(product => {
//             return product.destroy();
//         })
//         .then(result => {
//             console.log('Product deleted!')
//             res.redirect('/admin/products');
//         })
//         .catch(err => {
//             console.log(err)
//         })
// }