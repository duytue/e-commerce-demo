const Product = require('../models/product');
// const Cart = require('../models/cart');

exports.getProducts = (req, res, next) => {
    Product.fetchAll()
        .then(products => {
            res.render('shop/product-list', {
                prods: products,
                pageTitle: 'All Products',
                path: '/products'
            });
        })
        .catch(err => {
            console.log(err)
        });
};

exports.getProduct = (req, res, next) => {
    const id = req.params.productId;

    Product.findById(id)
        .then(product => {
            console.log("inside getProduct", product)
            res.render('shop/product-detail', {
                pageTitle: product.title,
                path: '/products',
                product: product
            })
        })
        .catch(err => {
            console.log("getProduct", err)
        });
}

exports.getIndex = (req, res, next) => {
    Product.fetchAll()
        .then(products => {
            res.render('shop/index', {
                prods: products,
                pageTitle: 'Shop',
                path: '/',
                activeShop: true
            });
        })
        .catch(err => {
            console.log(err)
        });
}

exports.getCart = (req, res, next) => {
    req.user.getCart()
        .then(cart => {
            return cart.getProducts()
                .then(products => {
                    res.render('shop/cart', {
                        pageTitle: 'Carts',
                        path: '/cart',
                        products: products
                    });
                })
                .catch(err => console.log(err))
        })
        .catch(err => console.log(err))
};

exports.postCart = (req, res, next) => {
    const id = req.body.productId;
    let fetchedCart;
    let newQuantity = 1

    req.user.getCart()
        .then(cart => {
            fetchedCart = cart

            return cart.getProducts({ where: { id: id } })
        })
        .then(products => {
            if (products.length > 0) {
                let oldQuatity = products[0].cartItem.quantity;
                newQuantity = oldQuatity + 1
            }

            return Product.findById(id)
                .then(product => {
                    return fetchedCart.addProduct(product, { through: { quantity: newQuantity } });
                })
                .catch(err => console.log(err))
        })
        .then(() => {
            res.redirect('/cart');
        })
        .catch(err => console.log(err))
}

exports.getCheckout = (req, res, next) => {
    res.render('shop/checkout', {
        pageTitle: 'Checkout',
        path: '/checkout'
    })
};

exports.getOrders = (req, res, next) => {
    req.user.getOrders({ include: ['products'] })
        .then(orders => {
            res.render('shop/orders', {
                pageTitle: 'My Orders',
                path: '/orders',
                orders: orders
            });
        })
        .catch(err => console.log(err))
};

exports.postOrder = (req, res, next) => {
    let fetchedCart;
    req.user.getCart()
        .then(cart => {
            fetchedCart = cart
            return cart.getProducts()
        })
        .then(products => {
            return req.user.createOrder()
                .then(order => {
                    return order.addProducts(products.map(product => {
                        product.orderItem = { quantity: product.cartItem.quantity }

                        return product
                    }))
                })
                .catch(err => console.log(err))
        })
        .then(() => {
            return fetchedCart.setProducts(null);
        })
        .then(() => {
            res.redirect('/orders')
        })
        .catch(err => console.log(err))
}

exports.deleteCartItem = (req, res, next) => {
    const productId = req.body.productId;

    req.user.getCart()
        .then(cart => {
            return cart.getProducts({ where: { id: productId } })
        })
        .then(products => {
            return products[0].cartItem.destroy()
        })
        .then(result => {
            res.redirect('/cart');
        })
        .catch(err => console.log(err))
}